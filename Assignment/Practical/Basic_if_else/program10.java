/* real time example of if else ladder*/

class program10{
	public static void main(String[] args){
		int budget=6000000;
		System.out.println("Budget: " + budget);

		if(budget>=10000000)
			System.out.println("You can buy Farmhouse or Villa!");
		else if(budget>=7000000 && budget<10000000)
			System.out.println("You can buy 3BHK flat!");
		else if(budget>=5000000 && budget<7000000)
			System.out.println("You can buy 2BHK flat!");
		else if(budget>3000000 && budget<5000000)
			System.out.println("You can buy 1BHK flat!");
		else
			System.out.println("Sorry,Insufficient budget!");
	}
}


