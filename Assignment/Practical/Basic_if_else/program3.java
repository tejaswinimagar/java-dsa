/* print whether number is positive or negative */

class program3{
	public static void main(String[] args){
		int num=-9;
		System.out.println("Num: " +num);
		if(num>0)
			System.out.println(num + " is a positive number.");
		else if(num<0)
			System.out.println(num + " is a negative number.");
		else
			System.out.println(num + " is neither positive nor negative number.");
	}
}
