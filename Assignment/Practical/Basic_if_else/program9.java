/*Check pythagorean triplet or not */

class program9{
	public static void main(String[] args){
		int a=4;
		int b=5;
		int c=3;
		System.out.println("a: "+ a);
		System.out.println("b: "+ b);
		System.out.println("c: "+ c);

		if(a*a==b*b+c*c || b*b==a*a+c*c || c*c==a*a+b*b)
			System.out.println("Triangle is pythagorean triplet");
		else
			System.out.println("Triangle is not a pythagorean triplet");
	}
}

