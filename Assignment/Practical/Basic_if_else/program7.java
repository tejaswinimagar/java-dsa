/* Calculate profit loss*/

class program7{
	public static void main(String[] args){
		int sellP=500;
		int costP=550;
		System.out.println("Selling Price: " + sellP);
		System.out.println("Cost Price: " + costP);

		if(sellP<costP)
			System.out.println("Loss of " + (costP-sellP));
		else if(sellP>costP)
			System.out.println("Profit of " + (sellP-costP));
		else
			System.out.println("No profit,No loss");
	}
}


