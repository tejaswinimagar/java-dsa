/* If number is between 0 to 5 print it's spelling else print number is greater than 5*/

class program4{
	public static void main(String[] args){
		int num=4;
		System.out.println("Num: " + num);

		if(num<0)
			System.out.println("Number is less than five");
		else if(num==0)
			System.out.println("Zero");
		else if(num==1)
			System.out.println("One");
		else if(num==2)
			System.out.println("Two");
		else if(num==3)
			System.out.println("Three");
		else if(num==4)
			System.out.println("Four");
		else if(num==5)
			System.out.println("Five");
		else
			System.out.println("Number is greater than five");
	}
}
