/* find maximum between 3 numbers*/

class program6{
	public static void main(String[] args){
		int num1=5;
		int num2=7;
		int num3=5;
		System.out.println("Num 1: " + num1);
		System.out.println("Num 2: " + num2);
		System.out.println("Num 3: " + num3);

		if(num1>num2 && num1>num3)
			System.out.println(num1 + " is the maximum between " +num1+ "," +num2+ " and " +num3);
		else if(num2>num1 && num2>num3)
			System.out.println(num2 + " is the maximum between " +num1+ "," +num2+ " and " +num3);
		else if(num3>num1 && num3>num2)
			System.out.println(num3 + " is the maximum between " +num1+ "," +num2+ " and " +num3);
		else if(num1==num2 && num1>num3)
			System.out.println(num1 + " is the maximum between " +num1+ "," +num2+ " and " +num3);
		else if(num2==num3 && num2>num1)
			System.out.println(num2 + " is the maximum between " +num1+ "," +num2+ " and " +num3);
		else if(num1==num3 && num1>num2)
			System.out.println(num1 + " is the maximum between " +num1+ "," +num2+ " and " +num3);
		else
			System.out.println("All numbers are equal");
	}
}

